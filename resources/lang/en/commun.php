<?php

return [
    'details' => 'See',
    'modifier' => 'Edit',
    'enregistrer' => 'Save',
    'voir' => 'View',
    'supprimer' => 'Delete',
    'acceuil' => 'Home',
    'langue' => 'Language',
];