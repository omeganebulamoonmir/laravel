<table class="table">
    <thead>
        <tr>
            <th>{{ trans('etudiant.nom') }}</th>
            <th>{{ trans('etudiant.prenom') }}</th>
            <th>Action</th>
        </tr>
    </thead>
    
    <tbody>
        @foreach ($etudiants as $etudiant)
            <tr>
                <td>{{ $etudiant->nom }}</td>
                <td>{{ $etudiant->prenom }}</td>

                
                <td>
                    {!! Form::model($etudiant, ['method' => 'delete']) !!}

                    {!! Form::submit(
                        trans('commun.voir'), [
                            'formaction' => route('showEtudiant', $etudiant->id), 
                            'formmethod' => 'get', 
                            'name' => 'voir'
                        ]
                    ) !!}

                    {!! Form::submit(trans('commun.modifier'), [
                        'formaction' => route('editEtudiant', $etudiant->id), 
                        'formmethod' => 'get', 
                        'name' => 'modifier'
                        ]
                    ) !!}

                    {!! Form::submit(trans('commun.supprimer'), [
                        'formaction' => route('deleteEtudiant', $etudiant->id), 
                        'name' => 'supprimer'
                        ]
                    ) !!}

                    {!! Form::close() !!}                 
                </td>
            </tr>
        @endforeach
    </tbody>
</table>