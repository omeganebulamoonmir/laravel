@extends ('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">
                        {{ trans('etudiant.titreaffichage') }}
                    </div>
                    <div class="card-body">
                        <dl>
                            <dt>{{ trans('etudiant.nom') }}:</dt>
                            <dd>{{ $etudiant->nom }}</dd>
                            <dt>{{ trans('etudiant.prenom') }}:</dt>
                            <dd>{{ $etudiant->prenom }}</dd>
                        </dl>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection