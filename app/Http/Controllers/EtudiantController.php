<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Etudiant;
use App\Http\Requests\EtudiantRequest;

class EtudiantController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function add(EtudiantRequest $request)
    {
        Etudiant::create($request->all());

        return back()->with('status', trans('etudiant.msgenregistrementok'));
    }

    public function show($id)
    {
        $etudiant = Etudiant::findorfail($id);

        return view('etudiant.show', compact('etudiant'));
    }

    public function edit($id)
    {
        $etudiant = Etudiant::findorfail($id);

        return view('etudiant.edit', compact('etudiant'));
    }

    public function update(EtudiantRequest $request, $id)
    {
        $etudiant = Etudiant::findorfail($id);
        $etudiant->nom = $request->input('nom');
        $etudiant->prenom = $request->input('prenom');
        $etudiant->save();
        
        return back()->with('status', trans('etudiant.msgmiseajourok'));
    }

    public function delete($id)
    {
        Etudiant::destroy($id);

        return back()->with('status', trans('etudiant.msgsupprimerok'));
    }

}
